package com.spring.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.spring.app.entity.Student;

@Repository("studentRepository")
public interface StudentRepository extends CrudRepository<Student, Integer>{

}
