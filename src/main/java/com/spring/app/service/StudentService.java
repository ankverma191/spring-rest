package com.spring.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.app.entity.Student;
import com.spring.app.repository.StudentRepository;

import lombok.extern.slf4j.Slf4j;

@Service("studentService")
@Slf4j
public class StudentService {
	
	@Autowired
	private StudentRepository studentRepository = null;
	
	public List<Student> students() {
		log.info("called services");
		return (List<Student>) studentRepository.findAll();
	}

	public Student students(int id) {
		final Optional<Student> optional =  studentRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new RuntimeException("No data found for id [ " + id + " ] ");
		}
	}

}
