package com.spring.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Student {
	@Id
	@GeneratedValue
	private Integer id = null;
	private String name = null;
	private Integer age = null;
	private String emailId = null;
}
