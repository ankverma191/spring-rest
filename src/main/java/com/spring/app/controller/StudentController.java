package com.spring.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.app.entity.Student;
import com.spring.app.service.StudentService;

@RestController
@RequestMapping("/students")
public class StudentController {

	@Autowired
	private StudentService studentService = null;

	@ResponseBody
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Student>> students() {
		return new ResponseEntity<List<Student>>(studentService.students(), HttpStatus.OK);
	}
	
	@ResponseBody
	@GetMapping(path = "/{id}" , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Student> students(@PathVariable final int id) {
		return new ResponseEntity<Student>(studentService.students(id), HttpStatus.OK);
	}

}
